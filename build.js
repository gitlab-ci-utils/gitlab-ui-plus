import * as esbuild from 'esbuild';
import * as fs from 'node:fs/promises';
import path from 'node:path';

const sourceDirectory = 'src';
const destinationDirectory = 'dist';
const packageFileName = 'package.json';
const manifestFileName = 'manifest.json';
const defaultEsbuildOptions = { bundle: true };

const contentScript = 'content/content.js';
const files = ['content/content.css'];

/**
 * Removes a directory and all its contents.
 *
 * @param {string} directory The directory to remove.
 * @async
 * @static
 * @private
 */
const removeDirectory = async (directory) => {
    await fs.rm(directory, { force: true, recursive: true });
};

/**
 * Creates the given directory if it does not exist.
 *
 * @param {string} directory The directory to create.
 * @async
 * @static
 * @private
 */
const ensureDirectory = async (directory) => {
    await fs.mkdir(directory, { recursive: true });
};

/**
 * Copies a file from the source directory to the destination directory,
 * creating any necessary subdirectories.
 *
 * @param {string} relativePath The file path relative to the source directory.
 * @async
 * @static
 * @private
 */
const copyFile = async (relativePath) => {
    const sourcePath = path.resolve(sourceDirectory, relativePath);
    const destinationPath = path.resolve(destinationDirectory, relativePath);
    const destinationDirectoryPath = path.dirname(destinationPath);

    // Copy fails if the destination directory does not exist
    await ensureDirectory(destinationDirectoryPath);
    await fs.copyFile(sourcePath, destinationPath);
};

/**
 * Gets the JSON data from the given file.
 *
 * @param   {string} filePath The path to the JSON path.
 * @returns {object}          The parsed JSON data.
 * @async
 * @static
 * @private
 */
const getJsonData = async (filePath) =>
    JSON.parse(await fs.readFile(filePath, 'utf8'));

/**
 * Bundles the given script using esbuild and writes to the output directory.
 *
 * @param {string} scriptRelativePath The path to the script to bundle
 *                                    relative to the source directory.
 * @param {object} options            ESBuild build options.
 * @async
 * @static
 * @private
 */
const bundleScript = async (scriptRelativePath, options = {}) => {
    const fullScriptPath = path.resolve(sourceDirectory, scriptRelativePath);
    const scriptDestinationDirectory = path.dirname(
        path.resolve(destinationDirectory, scriptRelativePath)
    );
    await esbuild.build({
        entryPoints: [fullScriptPath],
        outdir: scriptDestinationDirectory,
        ...options
    });
};

/**
 * Creates the final manifest.json, merging data from package.json,
 * and write to the output directory.
 *
 * @async
 * @static
 * @private
 */
const createMergedManifest = async () => {
    const packageJson = await getJsonData(packageFileName);
    const manifestJson = await getJsonData(
        path.resolve(sourceDirectory, manifestFileName)
    );
    const { description, name, version } = packageJson;
    const mergedManifest = { description, name, version, ...manifestJson };

    await fs.writeFile(
        path.resolve(destinationDirectory, manifestFileName),
        JSON.stringify(mergedManifest)
    );
};

/**
 * Builds the extension.
 *
 * @async
 * @static
 * @private
 */
const build = async () => {
    await removeDirectory(destinationDirectory);
    await ensureDirectory(destinationDirectory);
    for (const file of files) {
        // eslint-disable-next-line no-await-in-loop -- avoid race conditions
        await copyFile(file);
    }
    bundleScript(contentScript, defaultEsbuildOptions);
    createMergedManifest();
};

await build();
