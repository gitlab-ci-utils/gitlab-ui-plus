import { PageTypes } from '../../src/lib/pages.js';

/**
 * Sets up the <body> to simulate a specified page.
 *
 * @param {PageTypes} [pageType] The page type to emulate.
 */
const setBodyByPageType = (pageType) => {
    const body = document.createElement('body');
    const excludedPages = [PageTypes.All, PageTypes.Unknown];
    if (
        Object.values(PageTypes).includes(pageType) &&
        !excludedPages.includes(pageType)
    ) {
        body.dataset.page = 'projects:pipelines:show';
    }
    document.body = body;
};

/**
 * Resets the document to an empty state.
 */
const resetDocument = () => {
    const body = document.querySelector('body');
    body.remove();
};

/**
 * Adds an element with the given attributes to the Dob body.
 *
 * @param {string} parentElement The query selector for the element to add to.
 * @param {string} element       The type of element to add.
 * @param {object} attributes    The attributes/values to add to the element.
 */
const addElementToParent = (
    parentElement,
    element = 'div',
    attributes = {}
) => {
    const domElement = document.createElement(element);
    for (const [key, value] of Object.entries(attributes)) {
        domElement.setAttribute(key, value);
    }
    document.querySelector(parentElement).append(domElement);
};

export { addElementToParent, resetDocument, setBodyByPageType };
