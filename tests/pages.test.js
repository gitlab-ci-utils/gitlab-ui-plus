import {
    PageTypes,
    registerPageAction,
    waitForElement
} from '../src/lib/pages.js';

import {
    addElementToParent,
    resetDocument,
    setBodyByPageType
} from './helpers/dom.js';

import {
    afterAll,
    afterEach,
    beforeAll,
    beforeEach,
    describe,
    expect,
    it,
    vi
} from 'vitest';

describe('registerPageAction', () => {
    const testAction = vi.fn();

    beforeEach(() => {
        vi.spyOn(console, 'log').mockImplementation(() => {});
    });

    afterEach(() => {
        resetDocument();
        vi.resetAllMocks();
    });

    afterAll(() => {
        vi.restoreAllMocks();
    });

    it('should execute action for PageTypes All', async () => {
        setBodyByPageType(PageTypes.Unknown);
        await registerPageAction(PageTypes.All, testAction);
        expect(testAction).toHaveBeenCalledWith();
    });

    it('should execute action for PageType PipelineDetails on pipeline details page', async () => {
        setBodyByPageType(PageTypes.PipelineDetails);
        await registerPageAction(PageTypes.PipelineDetails, testAction);
        expect(testAction).toHaveBeenCalledWith();
    });

    it('should not execute action for PageType PipelineDetails on other pages', async () => {
        setBodyByPageType(PageTypes.Unknown);
        await registerPageAction(PageTypes.PipelineDetails, testAction);
        expect(testAction).not.toHaveBeenCalled();
    });
});

describe('waitForElement', () => {
    const attributes = { 'data-foo': 'bar' };
    const selector = '[data-foo=bar]';
    const interval = 100;
    const duration = 1000;

    beforeAll(() => {
        vi.spyOn(console, 'log').mockImplementation(() => {});
    });

    afterEach(() => {
        resetDocument();
        vi.resetAllMocks();
    });

    afterAll(() => {
        vi.restoreAllMocks();
    });

    it('should resolve with element if element found on page', async () => {
        setBodyByPageType(PageTypes.PipelineDetails);
        addElementToParent('body', 'div', attributes);
        await expect(
            waitForElement(selector, interval, duration)
        ).resolves.toStrictEqual(document.querySelector(selector));
    });

    it('should reject if element not found on page after duration', async () => {
        setBodyByPageType(PageTypes.PipelineDetails);
        const start = performance.now();
        await expect(
            waitForElement(selector, interval, duration)
        ).rejects.toThrow(selector);
        const end = performance.now();
        expect(end - start).toBeGreaterThanOrEqual(duration);
    });
});
