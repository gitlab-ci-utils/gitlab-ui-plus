import { PageTypes, registerPageAction } from '../lib/pages.js';
import { navBarAction } from '../lib/nav-bar.js';
import { pipelineDetailsAction } from '../lib/pipelines.js';

/**
 * Main executive function for the extension.
 *
 * @async
 * @static
 * @public
 */
const main = async () => {
    console.log('GitLab UI Plus is here!');
    await registerPageAction(PageTypes.All, navBarAction);
    await registerPageAction(PageTypes.PipelineDetails, pipelineDetailsAction);
};

main().catch((error) => console.error(error));
