/**
 * Actions to take on pipeline pages.
 *
 * @module pipeline
 */

import { waitForElement } from '../lib/pages.js';

/**
 * Click the "Show links" switch on pipeline details pages to show job
 * relationships.
 *
 * @async
 * @static
 * @public
 */
const pipelineDetailsAction = async () => {
    try {
        const element = await waitForElement(
            '[data-testid="show-links-toggle"] button'
        );
        if (element?.ariaChecked === 'false') {
            console.log('Showing job links');
            element.click();
        }
    } catch (error) {
        console.log(error);
    }
};

// ShowJobLinks

export { pipelineDetailsAction };
