/**
 * Functions for interacting with page elements.
 *
 * @module pages
 */

/**
 * Enumeration of page types. The value should be the data-page attribute value
 * for the page <body> tag.
 *
 * @enum {string}   PageTypes
 */
const PageTypes = Object.freeze({
    All: 'all',
    PipelineDetails: 'projects:pipelines:show',
    Unknown: 'unknown'
});

let currentPageType;

/**
 * Determines the page type.
 *
 * @returns {string} The page type, or 'unknown'.
 * @static
 * @private
 */
const getPageType = () => {
    const body = document.querySelector('body');
    if (body) {
        const { page } = body.dataset;
        currentPageType =
            page === PageTypes.PipelineDetails
                ? PageTypes.PipelineDetails
                : PageTypes.Unknown;
    }
    console.log('Page type:', currentPageType);
    return currentPageType;
};

/**
 * Register a function to execute on a specified page, or on all pages.
 *
 * @param {PageTypes} pageType The page type to register the action for.
 * @param {Function}  action   The function to execute on the specified page.
 * @async
 * @static
 * @public
 */
const registerPageAction = async (pageType, action) => {
    if (pageType === PageTypes.All || getPageType() === pageType) {
        await action();
    }
};

const defaultInterval = 100;
const defaultDuration = 5000;

/**
 * Waits for an element to appear in the DOM.
 *
 * @param   {string}           selector The selector of the element to wait for.
 * @param   {number}           interval The interval in milliseconds to check for the element.
 * @param   {number}           duration The duration in milliseconds to check for the element.
 * @returns {Promise<Element>}          The DOM element.
 * @async
 * @static
 * @public
 */
const waitForElement = async (
    selector,
    interval = defaultInterval,
    duration = defaultDuration
    // eslint-disable-next-line require-await -- returns promise
) =>
    // eslint-disable-next-line promise/avoid-new -- promisify setInterval
    new Promise((resolve, reject) => {
        const retryLimit = duration / interval;
        let retryCount = 0;
        const intervalId = setInterval(() => {
            if (document.querySelector(selector)) {
                console.log('Found element', selector);
                clearInterval(intervalId);
                resolve(document.querySelector(selector));
            } else {
                console.log('Element not found', selector);
                retryCount++;
                if (retryCount > retryLimit) {
                    clearInterval(intervalId);
                    reject(new Error(`Failed to find element ${selector}`));
                }
            }
        }, interval);
    });

export { PageTypes, registerPageAction, waitForElement };
