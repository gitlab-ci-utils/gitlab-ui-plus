/**
 * Functions to modify behavior of the nav bar.
 *
 * @module nav-bar
 */

/**
 * If the Admin link exists in the contexts menu, copy it to the top of the
 * nav bar.
 *
 * @static
 * @public
 */
const copyAdminLinktoNavBar = () => {
    const adminLink = document.querySelector(
        '[data-qa-submenu-item="Admin Area"]'
    );
    if (adminLink) {
        try {
            const a = document.createElement('a');
            a.setAttribute('href', adminLink.href);
            a.setAttribute('title', 'Admin Area');
            a.classList = document.querySelector(
                '[data-testid="super-sidebar-search-button"]'
            ).classList;
            a.style.padding = '0.5rem';

            const icon = adminLink.querySelector('svg').cloneNode(true);
            icon.style.marginLeft = '0';
            a.append(icon);

            const container = document.querySelector('.user-bar > div > div');
            container.style.textAlign = 'center';
            container.append(a);
            console.log('Added admin link to nav bar');
        } catch {
            console.error('Error adding Admin link to nav bar');
        }
    }
};

export { copyAdminLinktoNavBar as navBarAction };
