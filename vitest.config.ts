// eslint-disable-next-line n/file-extension-in-import -- Named export
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    coverage: {
      all: true,
      include: ['src/**'],
      reporter: ['text', 'html', 'json']
    },
    environment: 'jsdom',
    exclude: ['**/archive/**', '**/dist/**', '**/node_modules/**'],
    outputFile: 'junit.xml',
    reporters: ['verbose', 'junit']
  }
});
