### Summary

<!--
Summarize the bug and how it can be reproduced. If necessary, include a link to an example project that exhibits the behavior.
-->

### Environment

- Extension version:
- Operating system:
- Browser:
- Browser version:

/label ~Bug
