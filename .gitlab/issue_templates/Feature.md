### What problem do you want to solve?

<!-- Please explain your use case in as much detail as possible. -->

### What is the proposed solution?

<!-- Please explain how you'd like to the problem to be addressed. -->

### Application version

<!-- What version of the application are you currently using? -->

### Additional details

<!-- Is there any additional information that's relevant to this proposed feature? -->

/label ~Feature
