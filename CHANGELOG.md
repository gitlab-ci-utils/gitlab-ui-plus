# Changelog

## Unreleased

### Changed

- Display GitLab profile theme color in the header bar. (#1)
- Contract vertical spacing in nav bar. (#2)
- Contracts vertical spacing on pipeline list and pipeline graph pages. (#3)
- Expand horizontal spacing on pipeline details page header for pipeline jobs,
  compute credits, and pipeline duration. (#3)
- On pipeline detail page, checked switch to show job dependencies. (#4)
- If user is an instance Admin, adds an "Admin" link to the header bar. (#8)

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#16)
