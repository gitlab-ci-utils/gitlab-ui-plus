import { expect } from '@playwright/test';
import { test } from './helpers/fixtures.js';

test.describe('pipeline page actions', () => {
    test('should check the job dependencies button on pipeline details pages', async ({
        page
    }) => {
        const buttonSelector = '[data-testid="show-links-toggle"] button';
        await page.goto(
            `https://gitlab.com/gitlab-ci-utils/gitlab-ui-plus/-/pipelines/${process.env.CI_PIPELINE_ID}`
        );
        // Ensure job dependencies are shown so button is visible
        await page.getByText('Job dependencies').click();
        /* eslint-disable-next-line playwright/no-raw-locators -- no available
           sematic locators, limited by GitLab HTML. */
        await expect(page.locator(buttonSelector)).toBeVisible();
        // Button is unchecked by default, but extension should check it
        /* eslint-disable-next-line playwright/no-raw-locators -- no available
           sematic locators, limited by GitLab HTML. */
        await expect(page.locator(buttonSelector)).toHaveAttribute(
            'aria-checked',
            'true'
        );
    });
});
