import { test as base, chromium } from '@playwright/test';
import path from 'node:path';

/**
 * Extends the base test function with persistent context so that
 * extensions can be loaded.
 */
export const test = base.extend({
    // eslint-disable-next-line no-empty-pattern -- convention
    context: async ({}, use) => {
        const pathToExtension = path.resolve('./dist');
        const context = await chromium.launchPersistentContext('', {
            args: [
                `--headless=new`,
                `--disable-extensions-except=${pathToExtension}`,
                `--load-extension=${pathToExtension}`
            ],
            headless: false
        });
        await use(context);
        await context.close();
    },
    extensionId: async ({ context }, use) => {
        let [background] = context.serviceWorkers();
        if (!background)
            background = await context.waitForEvent('serviceworker');
        // eslint-disable-next-line prefer-destructuring, no-magic-numbers -- less intuitive
        const extensionId = background.url().split('/')[2];
        await use(extensionId);
    }
});
