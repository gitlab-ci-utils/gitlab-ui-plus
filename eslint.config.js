import globals from 'globals';
import recommendedConfig from '@aarongoldenthal/eslint-config-standard/recommended-esm.js';

export default [
    ...recommendedConfig,
    {
        files: ['**/*.js'],
        languageOptions: {
            globals: {
                ...globals.browser,
                ...globals.chrome
            }
        },
        name: 'browser'
    },
    {
        files: ['src/**/*.js'],
        name: 'FIX ME',
        rules: {
            'promise/no-multiple-resolved': 'off'
        }
    },
    {
        files: ['src/content/content.js'],
        name: 'iife',
        rules: {
            'promise/prefer-await-to-callbacks': 'off',
            'promise/prefer-await-to-then': 'off',
            'unicorn/prefer-top-level-await': 'off'
        }
    },
    {
        files: ['playwright.config.js'],
        name: 'playwright types',
        rules: { 'jsdoc/imports-as-dependencies': 'off' }
    },
    {
        ignores: [
            '.vscode/**',
            'archive/**',
            'dist/**',
            'node_modules/**',
            'coverage/**',
            'pagean-external-scripts/**',
            'playwright-report/**'
        ],
        name: 'ignores'
    }
];
