# GitLab UI Plus

Chrome browser extension to improve GitLab UI/UX.

## Features

- On pipeline details page automatically checks "Show dependencies" if
  displayed (when `Group jobs by` `Job dependencies`).
- If user is an instance Admin, adds an Admin Area link to the top of the nav bar.
- Styling improvements:
  - Expands horizontal spacing on pipeline details page header for pipeline jobs,
    compute credits, and pipeline duration.
